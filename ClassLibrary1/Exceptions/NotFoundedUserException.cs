﻿using System;
using System.Runtime.Serialization;

namespace ClassLibrary1.Exceptions
{
    [Serializable]
    public class NotFoundedUserException:Exception
    {
        public NotFoundedUserException() : base() { }
        public NotFoundedUserException(string name) : base($" User with name:{name} doesn't exist") { }
        protected NotFoundedUserException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
