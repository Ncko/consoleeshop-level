﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary1
{
    [Serializable]
    public class NotFoundedGoodException:Exception
    {
        public NotFoundedGoodException() : base() { }
        public NotFoundedGoodException(string name) : base($"Good with name:{name} doesn't exist") { }
        protected NotFoundedGoodException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
