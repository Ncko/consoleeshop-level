﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary1.Exceptions
{
    [Serializable]
    public class RoleNotFoundException:Exception
    {
        public RoleNotFoundException() : base() { }
        public RoleNotFoundException(string mes) : base(mes) { }
        protected RoleNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
