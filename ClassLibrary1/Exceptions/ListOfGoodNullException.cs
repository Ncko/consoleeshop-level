﻿using System;
using System.Runtime.Serialization;

namespace ClassLibrary1.Exceptions
{
    [Serializable]
    public class ListOfGoodNullException:Exception
    {
        public ListOfGoodNullException() : base() { }
        public ListOfGoodNullException(string mes) : base(mes) { }
        protected ListOfGoodNullException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
