﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassLibrary1.Interfaces;

namespace ClassLibrary1
{
    public class Context
    {
        public IMenuDisplayable ContextMenu { get; set; }
        public Context(IMenuDisplayable _menu)
        {
            ContextMenu = _menu;
        }
        public void DisplayMenu(List<RegisterUser> users,List<AdministratorUser> admins,List<Good> goods)
        {
            ContextMenu = ((IMenuDisplayable)ContextMenu.MenuDisplay(users, admins, goods));
        }
    }
}
