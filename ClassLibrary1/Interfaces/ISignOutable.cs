﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface ISignOutable
    {
        bool SignOut()
        {
            Console.WriteLine("  Are you sure that you want to sign out ?");
            Console.WriteLine("             y / n                 ");

            return Console.ReadLine() == "y";
        }

    }
}
