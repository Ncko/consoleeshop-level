﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Interfaces
{
    public interface IMenuDisplayable
    {
        UserRole MenuDisplay(List<RegisterUser> users, List<AdministratorUser> admins, List<Good> goods);
    }
}
