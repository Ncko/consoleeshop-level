﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public class RegisterUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public List<Order> Orders { get; set; }
        public override string ToString()
        {
            return $" Name: {Name} \n" +
            $"  Surname: {Surname} \n" +
            $"  Nickname: {NickName} \n" +
            $"  Password: {Password} \n" +
            $"  Email: {Email} \n";
        }

    }
}
