﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public enum Status
    {
        New=0,
        CanceledByAdministrator=1,
        PaymentReceived=2,
        Sent=3,
        Received=4,
        Completed=5,
        CanceledByUser=6
    }
}
