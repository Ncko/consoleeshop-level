﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    enum ChangeUser
    {
        ChangeName = 1,
        ChangeSurname = 2,
        ChangeNickName = 3,
        ChangePassword = 4,
        ChangeEmail = 5,
        Exit = 6
    }

}
