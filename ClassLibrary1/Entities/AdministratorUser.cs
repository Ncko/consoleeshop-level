﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public class AdministratorUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public string AdminPassword { get; private set; } = "admin4444";
    }
}
