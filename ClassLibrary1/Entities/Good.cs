﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public class Good
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }

        public override string ToString()
        {
            return $"  Name: {Name} \n"+
                   $"  Category: {Category} \n"+
                   $"  Description: {Description} \n"+
                   $"  Cost: {Cost} \n";
        }
    }
}
