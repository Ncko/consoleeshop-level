﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassLibrary1.Exceptions;
using ClassLibrary1.Interfaces;

namespace ClassLibrary1
{
    public class RegisterUserRole : UserRole, ISignOutable,IMenuDisplayable
    {
        public Action<List<Good>, List<RegisterUser>> _createNewOrder { get; private set; }
        public Action<List<RegisterUser>> _cancelOrder { get; private set; }
        public Action<List<RegisterUser>> _displayMyOrders { get; private set; }
        public Action<List<RegisterUser>> _setStatusReceived { get; private set; }
        public Action<List<RegisterUser>> _changePersonalInformation { get; private set; }

        public delegate bool SignOut();
        public SignOut _signOut { get; private set; }

        public enum Operations
        {
            DisplayGoods = 1,
            SearchGoodByName = 2,
            CreateNewOrder = 3,
            CancelNewOrder = 4,
            DisplayOrders = 5,
            SetReceived = 6,
            ChangePersonalInformation = 7,
            SignOut = 8
        }
        private static string _menu = "               == You entered in the shop  by the registered user ==        \n" +
                                      "      |1|   Display list of goods         |2|    Search good by name   \n" +
                                      "      |3|   Create new order              |4|    Cancel order   \n" +
                                      "      |5|   Display my orders             |6|    Set status received on order \n" +
                                      "      |7|   Change personal information   |8|    Sign out  \n";

        public RegisterUserRole() :base()
        {
            _createNewOrder = CreateNewOrder;
            _cancelOrder = CancelingOrder;
            _displayMyOrders = ViewHistoryOfOrders;
            _setStatusReceived = SettingStatusReceived;
            _changePersonalInformation = ChangePersonalInformationn;
            _signOut = ((ISignOutable)this).SignOut;
        }
        public RegisterUserRole(string nickname):this()
        {
            NickName = nickname;
           
        }
        public UserRole MenuDisplay(List<RegisterUser> users, List<AdministratorUser> admins, List<Good> goods)
        {
           
            int operation = - 1;

            while (operation != (int)Operations.SignOut)
            {
                Console.WriteLine(_menu);
                Console.WriteLine("    Choose operation:  ");
                string op = Console.ReadLine();

                while (!Int32.TryParse(op, out operation) || operation < 1 || operation > 8)
                {
                    Console.WriteLine("    You entered wrong number . Please enter number of according to the operation ");
                    Console.Write("    Choose operation:  ");
                    op = Console.ReadLine();
                }

                switch (operation)
                {
                    case (int)Operations.DisplayGoods:
                        _displayListOfGoods(goods);
                        break;
                    case (int)Operations.SearchGoodByName:
                        _displaySearchedGood(goods);
                        break;
                    case (int)Operations.CreateNewOrder:
                        _createNewOrder(goods, users);
                        break;
                    case (int)Operations.CancelNewOrder:
                        _cancelOrder(users);
                        break;
                    case (int)Operations.ChangePersonalInformation:
                        _changePersonalInformation(users);
                        break;
                    case (int)Operations.SetReceived:
                        _setStatusReceived(users);
                        break;
                    case (int)Operations.DisplayOrders:
                        _displayMyOrders(users);
                        break;
                    case (int)Operations.SignOut:
                        if (!(_signOut()))
                            operation = -1;
                        else
                        {
                            Console.Clear();
                            return new GuestUserRole();
                        }
                        break;
                }
            }
            return new GuestUserRole();

        }
        public static List<string> SelectCategory(List<Good> goods)
        {
            if(goods == null)
            {
                throw new ListOfGoodNullException("  List of goods is null !");
            }    
            List<string> result = new List<string>();

            foreach (Good good in goods)
            {
                if (result.Find(t => t == good.Category) == null)
                    result.Add(good.Category);
            }
            return result;
        }

        public void CreateNewOrder(List<Good> goods, List<RegisterUser> users)
        {
            Console.WriteLine("  --Choose category of goods--  \n");
            

            List<string> category = SelectCategory(goods);

            for (int i = 0; i < category.Count; i++)
            {
                Console.WriteLine($" {i + 1}.  {category[i]}");
            }

            Console.Write("  Input number of category ");
            string chosen = Console.ReadLine();
            int chosenNumber;

            while (!Int32.TryParse(chosen, out chosenNumber) || chosenNumber - 1 < 0 || chosenNumber - 1 >= category.Count)
            {
                Console.WriteLine("  You inputed invalid number or number out of range");
                chosen = Console.ReadLine();
            }
            chosenNumber = Convert.ToInt32(chosen) - 1;
            chosen = category[chosenNumber];

            List<Good> goodsByChosenCategory = new List<Good>();

            Console.WriteLine("  --Choose good--  \n");
            

            int j = 0;
            foreach (Good good in goods)
            {
                if (good.Category == chosen)
                {
                    Console.WriteLine($" {j + 1}.  {good.Name}");
                    Console.WriteLine($" Description: {good.Description}");
                    Console.WriteLine($" Cost: {good.Cost}");
                    Console.WriteLine();
                    goodsByChosenCategory.Add(new Good { Name = good.Name, Description = good.Description, Cost = good.Cost });
                    j++;
                }
            }

            Console.Write("  Input number of good ");
            chosen = Console.ReadLine();
            while (!Int32.TryParse(chosen, out chosenNumber) || chosenNumber - 1 < 0 || chosenNumber - 1 >= goodsByChosenCategory.Count)
            {
                Console.WriteLine("  You inputed invalid number or number out of range");
                chosen = Console.ReadLine();
            }
            chosenNumber = Convert.ToInt32(chosen) - 1;
            Console.WriteLine();

            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].NickName == NickName)
                    users[i].Orders.Add(new Order { good = goodsByChosenCategory[chosenNumber], CurrentStatus = Status.New, CreationDate = DateTime.Now });
            }

        }

        public int FindMyself(List<RegisterUser> users)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].NickName == NickName)
                {
                    return i;
                }
            }

            throw new NotFoundedUserException(NickName);
        }

        public void ViewHistoryOfOrders(List<RegisterUser> users)
        {
            int id = FindMyself(users);

            for (int i = 0; i < users[id].Orders.Count; i++)
            {
                Console.WriteLine($" Id:{i + 1} Good: {users[id].Orders[i].good.Name} ,Price: {users[id].Orders[i].good.Cost} Status: {users[id].Orders[i].CurrentStatus.ToString()}");
                Console.WriteLine($" Date of creation: {users[id].Orders[i].CreationDate} \n");

            }
        }

        public void ChangePersonalInformationn(List<RegisterUser> users)
        {
            int id = FindMyself(users);

            int Change = 1;
            string name, surname, nickname, password, email;
            while (Change != (int)ChangeUser.Exit)
            {
                Console.WriteLine("       ---Change personal inforamtion---    ");
                Console.WriteLine("   |1|  Change name      |2|  Change surname ");
                Console.WriteLine("   |3|  Change nickname  |4|  Change password ");
                Console.WriteLine("   |5|  Change email     |6|  Close menu ");
                Change = Convert.ToInt32(Console.ReadLine());
                switch (Change)
                {
                    case (int)ChangeUser.ChangeName:
                        Console.Write("   Enter your name: ");
                        name = Console.ReadLine();
                        users[id].Name = name;
                        Console.WriteLine();
                        break;
                    case (int)ChangeUser.ChangeSurname:
                        Console.Write("   Enter your surname: ");
                        surname = Console.ReadLine();
                        users[id].Surname = surname;
                        Console.WriteLine();
                        break;
                    case (int)ChangeUser.ChangeNickName:
                        Console.Write("   Enter your nickname: ");
                        nickname = Console.ReadLine();
                        users[id].NickName = nickname;
                        NickName = nickname;
                        Console.WriteLine();
                        break;
                    case (int)ChangeUser.ChangePassword:
                        Console.Write("   Enter your password: ");
                        password = Console.ReadLine();
                        users[id].Password = password;
                        Console.WriteLine();
                        break;
                    case (int)ChangeUser.ChangeEmail:
                        Console.Write("   Enter your email: ");
                        email = Console.ReadLine();
                        users[id].Email = email;
                        Console.WriteLine();
                        break;
                    case (int)ChangeUser.Exit:
                        break;
                }

            }
            Console.WriteLine();
        }
        public void CancelingOrder(List<RegisterUser> users)
        {
            int userId = FindMyself(users);
            Console.WriteLine("   Enter Id of Order  ");
            string input = Console.ReadLine();
            int Id;
            while (!Int32.TryParse(input, out Id) || Id - 1 < 0 || Id - 1 >= users[userId].Orders.Count)
            {
                Console.WriteLine("   You entered invalid Id of order ");
                Console.WriteLine("   Enter Id of Order again ");
                input = Console.ReadLine();
            }
            Id = Id - 1;

            Console.WriteLine();
            while (Id > users[userId].Orders.Count || Id < 0)
            {
                Console.WriteLine("   You entered invalid Id of order ");
                Console.WriteLine("   Enter Id of Order again ");
                Id = Convert.ToInt32(Console.ReadLine());
            }
            if (users[userId].Orders[Id].CurrentStatus != Status.Received || users[userId].Orders[Id].CurrentStatus != Status.PaymentReceived)
            {
                users[userId].Orders.RemoveAt(Id);
            }
            else
            {
                Console.WriteLine("   Order have alredy received ");
                Console.WriteLine("   You can't cancel this order!! ");
            }
        }


        public void SettingStatusReceived(List<RegisterUser> users)
        {
            int userId = FindMyself(users);

            Console.WriteLine("   Enter Id of Order that you want set to received ");
            string name = Console.ReadLine();
            int Id;
            Console.WriteLine();
            while (!Int32.TryParse(name, out Id) || Id - 1 >= users[userId].Orders.Count || Id - 1 < 0)
            {
                Console.WriteLine("   You enter invalid Id of order ");
                Console.WriteLine("   Enter Id of Order again ");
                name = Console.ReadLine();
            }
            Id = Id - 1;

            users[userId].Orders[Id].CurrentStatus = Status.Received;
        }

    }
}
