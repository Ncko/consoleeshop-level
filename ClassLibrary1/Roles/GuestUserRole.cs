﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassLibrary1.Interfaces;
namespace ClassLibrary1
{
    public class GuestUserRole: UserRole,IMenuDisplayable
    {
        public Action<List<RegisterUser>> _registration { get; private set; }
        public Func<List<RegisterUser>, string> _signIn { get; private set; }
        public Func<List<AdministratorUser>, string> _signInAsAdmin { get; private set; }


        private static string _menu = "             == You entered in the shop  by the Guest ==        \n" +
                                     "     |1|   Display list of goods       |2|    Search good by name   \n" +
                                     "     |3|   Registration                |4|    Sign in   \n" +
                                     "     |5|   Sign in as Administrator       \n";
        public enum Operations
        {
            DisplayGoods = 1,
            SearchGoodByName = 2,
            Registration = 3,
            SignIn = 4,
            SignInAsAdministrator = 5,

        }
      
        public GuestUserRole():base()
        {
            _registration+=Registration;
            _signIn += SignIn;
            _signInAsAdmin += SignInAsAdministrator;
        }

        public UserRole MenuDisplay(List<RegisterUser> users,List<AdministratorUser> admins,List<Good> goods)
        {

            int i = -1;

            while (i != (int)Operations.SignIn && i != (int)Operations.SignInAsAdministrator)
            {
                Console.WriteLine(_menu);
                string op = Console.ReadLine();

                while (!Int32.TryParse(op, out i) || i < 1 || i > 5)
                {
                    Console.WriteLine("    You entered wrong number . Please enter number of according to the operation ");
                    Console.Write("    Choose operation:  ");
                    op = Console.ReadLine();
                }
                switch (i)
                {
                    case (int)Operations.DisplayGoods:
                        _displayListOfGoods(goods);
                        break;
                    case (int)Operations.SearchGoodByName:
                        _displaySearchedGood(goods);
                        break;
                    case (int)Operations.Registration:
                        _registration(users);
                        break;
                    case (int)Operations.SignIn:
                        NickName = _signIn(users);
                        return new RegisterUserRole(NickName);
                    case (int)Operations.SignInAsAdministrator:
                        NickName = _signInAsAdmin(admins);
                        return new AdministratorUserRole(NickName);
                        
                }
            }
            if(i== (int) Operations.SignIn)
                return new RegisterUserRole(NickName);

            return new AdministratorUserRole(NickName);

        }

        public void Registration(List<RegisterUser> users)
        {
            Console.Write("  Input your name: ");
            string name = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input your surname: ");
            string surname = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input your nickname: ");
            string nickname = Console.ReadLine();
            Console.WriteLine();
            while (users.Find(t=>t.NickName==nickname)!=null)
            {
                Console.WriteLine("  User with this nickname already exist");
                Console.Write("  Input other nickname: ");
                nickname = Console.ReadLine();
                Console.WriteLine();
            }

            Console.Write("  Input your password: ");
            string password = Console.ReadLine();
            Console.WriteLine();

            while (password.Length < 6)
            {
                Console.WriteLine("  Your password is too short. Enter longer password");
                Console.Write("  Input your password: ");
                password = Console.ReadLine();
                Console.WriteLine();
            }


            Console.Write("  Input your email: ");
            string email = Console.ReadLine();
            Console.WriteLine();

            users.Add(new RegisterUser { Name = name, Surname = surname, NickName = nickname, Password = password, Email = email });

        }

        public string SignIn(List<RegisterUser> users )
        {
            Console.Write("  Input your nickname: ");
            string nickname = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input your password: ");
            string password = Console.ReadLine();
            Console.WriteLine();


            while (users.Find(t => t.NickName == nickname) == null || users.Find(t => t.NickName == nickname).Password!=password)
            {
                Console.WriteLine("  Your entered wrong nickname or password. Enter nickname and password again");
               
                Console.Write("  Input your nickname: ");
                nickname = Console.ReadLine();
                Console.WriteLine();

                Console.Write("  Input your password: ");
                password = Console.ReadLine();
                Console.WriteLine();

            }

            return nickname;
        }

        public string SignInAsAdministrator(List<AdministratorUser> admins)
        {
            Console.Write("  Input nickname: ");
            string nickname = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input password: ");
            string password = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input administrator password: ");
            string adminPassword = Console.ReadLine();
            Console.WriteLine();

            while (admins.Find(t => t.NickName == nickname) == null || admins.Find(t => t.NickName == nickname).Password != password 
                || admins.Find(t => t.NickName == nickname).AdminPassword != adminPassword)
            {
                Console.WriteLine("    Your entered wrong nickname or password or administrator password. Enter nickname and password again");

                Console.Write("  Input nickname: ");
                nickname = Console.ReadLine();
                Console.WriteLine();

                Console.Write("  Input password: ");
                password = Console.ReadLine();
                Console.WriteLine();

                Console.Write("  Input administrator password: ");
                adminPassword = Console.ReadLine();
                Console.WriteLine();
            }

            return nickname;
        }
    }
}
