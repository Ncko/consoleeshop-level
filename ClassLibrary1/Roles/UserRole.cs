﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public abstract class UserRole
    {
        public string NickName { get; protected set; }

        protected Action<List<Good>> _displayListOfGoods;
        protected Action<List<Good>> _displaySearchedGood;



        protected UserRole()
        {
            _displayListOfGoods += DisplayListOfGoods;
            _displaySearchedGood += DisplaySearchedGood;
        }
        public virtual Good SearchGoodByName(List<Good> goods,string name)
        {
            foreach (var good in goods)
            {
                if (good.Name == name)
                {
                    Console.WriteLine();
                    Console.WriteLine(good.ToString());
                    Console.WriteLine();
                    return good; 
                }
            }

            throw new NotFoundedGoodException(name);
        }

        public virtual void DisplaySearchedGood(List<Good> goods)
        {
            Console.Write("  Please enter name of good : ");
            string name = Console.ReadLine();
            try
            {
                SearchGoodByName(goods, name);
            }
            catch (NotFoundedGoodException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public virtual void DisplayListOfGoods(List<Good> goods)
        {
            Console.WriteLine("       == List of all goods ==   ");
            foreach (var good in goods)
            {
                Console.WriteLine();
                Console.WriteLine(good.ToString());
                Console.WriteLine();
            }
        }

    }
}
