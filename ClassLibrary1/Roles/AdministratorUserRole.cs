﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ClassLibrary1.Interfaces;

namespace ClassLibrary1
{
    public class AdministratorUserRole : UserRole, ISignOutable,IMenuDisplayable
    {
        public Action<List<Good>, List<RegisterUser>> _createNewOrder{get; private set;}

        public Action<List<RegisterUser>> _displayAndChangeUser { get; private set; }

        public Action<List<Good>> _addGood { get; private set; }

        public Action<List<Good>> _changeInformationGood { get; private set; }

        public Action<List<RegisterUser>> _changeStatusOfOrder { get; private set; }

        public delegate bool SignOut();
        public SignOut _signOut { get; private set; }

        private static string _menu = "               == You entered in the shop  by the administrator ==       \n" +
                              "     |1|   Display list of goods         |2|    Search good by name   \n" +
                              "     |3|   Create new order              |4|    Display and change user   \n" +
                              "     |5|   Add new good                  |6|    Change selected good   \n" +
                              "     |7|   Change status of order        |8|    Sign out  \n";

        public enum Operations
        {
            DisplayGoods = 1,
            SearchGoodByName = 2,
            CreateNewOrder = 3,
            DisplayAndChangeUsers = 4,
            AddGood = 5,
            ChangeGood = 6,
            ChangeStatusOfOrder = 7,
            SignOut = 8
        }
        enum ChangeGood
        {
            ChangeName = 1,
            ChangeCategory = 2,
            ChangeDescription = 3,
            ChangeCost = 4,
            Exit = 5
        }
        public AdministratorUserRole() :base() 
        {
            _createNewOrder = CreateNewOrder;
            _displayAndChangeUser = ChangeUsersInformation;
            _changeInformationGood = ChangeInformationGood;
            _changeStatusOfOrder = ChangeStatusOrder;
            _signOut = ((ISignOutable)this).SignOut;
            _addGood = AddGood;
        }
        public AdministratorUserRole(string nickname):this()
        {
            NickName = nickname;
        }
        public UserRole MenuDisplay(List<RegisterUser> users, List<AdministratorUser> admins, List<Good> goods)
        {
            int operation = -1;

            while (operation != (int)Operations.SignOut)
            {
                Console.WriteLine(_menu);
                Console.WriteLine("    Choose operation:  ");
                string op = Console.ReadLine();
                while (!Int32.TryParse(op, out operation) || operation < 1 || operation > 8)
                {
                    Console.WriteLine("    You entered wrong number . Please enter number of according to the operation ");
                    Console.Write("    Choose operation:  ");
                    op = Console.ReadLine();
                }

                switch (operation)
                {
                    case (int)Operations.DisplayGoods:
                        _displayListOfGoods(goods);
                        break;
                    case (int)Operations.SearchGoodByName:
                        _displaySearchedGood(goods);
                        break;
                    case (int)Operations.CreateNewOrder:
                        _createNewOrder(goods, users);
                        break;
                    case (int)Operations.DisplayAndChangeUsers:
                        _displayAndChangeUser(users);
                        break;
                    case (int)Operations.AddGood:
                        _addGood(goods);
                        break;
                    case (int)Operations.ChangeGood:
                        _changeInformationGood(goods);
                        break;
                    case (int)Operations.ChangeStatusOfOrder:
                        _changeStatusOfOrder(users);
                        break;
                    case (int)Operations.SignOut:
                        if (!_signOut())
                            operation = -1;
                        else
                        {
                            Console.Clear();
                            return new GuestUserRole();
                        }
                        break;
                }

            }

            return new GuestUserRole();
        }

        public void ChangeInformationGood(List<Good> goods)
        {

            Console.WriteLine("   --Choose the good what you wnat to change--  ");
            Console.WriteLine();
            DisplayListOfGoods(goods);
            Console.Write("   Enter name of good  ");
            string nameSelected = Console.ReadLine();
            while (goods.Find(t=>t.Name==nameSelected)==null)
            {
                Console.WriteLine("  You enter invalid number. Please enter again ");
                nameSelected = Console.ReadLine();
            }

            int Id = goods.FindIndex(t => t.Name == nameSelected);

            int Change = 1;
            string name, category, description, price;
            while (Change != (int)ChangeGood.Exit)
            {
                Console.WriteLine("         Change personal inforamtion    ");
                Console.WriteLine("  |1|  Change name         |2|  Change category ");
                Console.WriteLine("  |3|  Change description  |4|  Change price ");
                Console.WriteLine("  |5|  Close menu");
                Console.Write("   Enter number of operation: ");
                Change = Convert.ToInt32(Console.ReadLine());
                switch (Change)
                {
                    case (int)ChangeGood.ChangeName:
                        Console.Write("   Enter name: ");
                        name = Console.ReadLine();
                        goods[Id].Name = name;
                        break;
                    case (int)ChangeGood.ChangeCategory:
                        Console.Write("   Enter category: ");
                        category = Console.ReadLine();
                        goods[Id].Category = category;
                        break;
                    case (int)ChangeGood.ChangeDescription:
                        Console.Write("   Enter description: ");
                        description = Console.ReadLine();
                        goods[Id].Description = description;
                        break;
                    case (int)ChangeGood.ChangeCost:
                        Console.Write("   Enter price: ");
                        price = Console.ReadLine();
                        double i;
                        while(!Double.TryParse(price,out i) || i <= 0)
                        {
                            Console.Write("    Enter price: ");
                            price = Console.ReadLine();
                        }
                        goods[Id].Cost = Convert.ToInt32(price);
                        break;
                    case (int)ChangeGood.Exit:
                        break;
                }

            }
            Console.WriteLine();
        }

        public void AddGood(List<Good> goods)
        {
            Console.WriteLine("    --Creating new good--  ");
            Console.Write("  Input name of good : ");
            string name = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input category: ");
            string category = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input description: ");
            string description = Console.ReadLine();
            Console.WriteLine();

            Console.Write("  Input Price: ");
            string price = Console.ReadLine();
            double cost;
            while (!Double.TryParse(price,out cost) || cost <= 0)
            {
                Console.WriteLine("    Price must be bigger than zero and must be a number ");
                Console.Write("  Input Price: ");
                price = Console.ReadLine();
            }
            Console.WriteLine();    

            int choice;
            while (goods.Find(t => t.Name == name) != null)
            {
                Console.WriteLine("    Good with this name already exist");
                Console.WriteLine("  You want to replace or input new good?  ");
                Console.WriteLine("             1. replace/ 2. input new             ");
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 1)
                {
                    goods.Find(t => t.Name == name).Description = description;
                    goods.Find(t => t.Name == name).Category = category;
                    goods.Find(t => t.Name == name).Cost = cost;
                    return;
                }
                else
                {
                    Console.Write("  Input name of good : ");
                    name = Console.ReadLine();
                    Console.WriteLine();

                    Console.Write("  Input category: ");
                    category = Console.ReadLine();
                    Console.WriteLine();

                    Console.Write("  Input description: ");
                    description = Console.ReadLine();
                    Console.WriteLine();

                    Console.Write("  Input Price: ");
                    cost = Convert.ToDouble(Console.ReadLine());
                    while (cost <= 0)
                    {
                        Console.WriteLine("    Price must be bigger than zero  ");
                        Console.Write("  Input Price: ");
                        cost = Convert.ToDouble(Console.ReadLine());
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            goods.Add(new Good { Name = name, Category = category, Description = description, Cost = cost });
        }
        public void DisplayUsersInformation(List<RegisterUser> users)
        {
            Console.WriteLine("   --List of all registered users--  ");
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine($"{i+1}  {users[i].ToString()}");
                Console.WriteLine();
            }
        }
        public void ChangeUsersInformation(List<RegisterUser> users)
        {
            DisplayUsersInformation(users);

            Console.WriteLine("   ---Choose id of user which you want change---  ");
            Console.Write("    Enter id:  ");
            string idStr = Console.ReadLine();
            int id;
            while (!Int32.TryParse(idStr,out id) ||  id-1 < 0 || id - 1 >= users.Count)
            {
                Console.WriteLine("    You enter invalid id  ");
                Console.Write("    Enter Id: ");
                idStr = Console.ReadLine();
            }
            id = id - 1;

            int Change = 1;
            string name, surname, nickname, password, email;
            while (Change != (int)ChangeUser.Exit)
            {
                Console.WriteLine("    --Change personal inforamtion--    ");
                Console.WriteLine(" |1|  Change name      |2|  Change surname ");
                Console.WriteLine(" |3|  Change nickname  |4|  Change password ");
                Console.WriteLine(" |5|  Change email     |6|  Close menu ");
                Change = Convert.ToInt32(Console.ReadLine());
                switch (Change)
                {
                    case (int)ChangeUser.ChangeName:
                        Console.Write("   Enter your name: ");
                        name = Console.ReadLine();
                        users[id].Name = name;
                        break;
                    case (int)ChangeUser.ChangeSurname:
                        Console.Write("   Enter your surname: ");
                        surname = Console.ReadLine();
                        users[id].Surname = surname;
                        break;
                    case (int)ChangeUser.ChangeNickName:
                        Console.Write("   Enter your nickname: ");
                        nickname = Console.ReadLine();
                        users[id].NickName = nickname;
                        break;
                    case (int)ChangeUser.ChangePassword:
                        Console.Write("   Enter your password: ");
                        password = Console.ReadLine();
                        users[id].Password = password;
                        break;
                    case (int)ChangeUser.ChangeEmail:
                        Console.Write("   Enter your email: ");
                        email = Console.ReadLine();
                        users[id].Email = email;
                        break;
                    case (int)ChangeUser.Exit:
                        break;
                }

            }
            Console.WriteLine();
        }
        public void ChangeStatusOrder(List<RegisterUser> users)
        {
            DisplayUsersInformation(users);
            Console.WriteLine();
            Console.WriteLine("    ---Choose  user---   ");
            Console.Write("    Enter user id: ");
            string idStr = Console.ReadLine();
            int id;
            while (!Int32.TryParse(idStr,out id) || id - 1 < 0 || id - 1 >= users.Count)
            {
                Console.WriteLine("    You enter invalid id  ");
                Console.Write("    Enter user id: ");
                idStr = Console.ReadLine();
            }
            id = id - 1;

            for (int i = 0; i < users[id].Orders.Count; i++)
            {
                Console.WriteLine($" Id:{i + 1} Good: {users[id].Orders[i].good.Name} ,Price: {users[id].Orders[i].good.Cost} Status: {users[id].Orders[i].CurrentStatus.ToString()}");
                Console.WriteLine($" Date of creation: {users[id].Orders[i].CreationDate}");
                Console.WriteLine();
            }

            Console.WriteLine("    ---Choose  order---   ");
            Console.Write("    Enter order id: ");
            idStr = Console.ReadLine();
            int idOrder;
            while (!Int32.TryParse(idStr,out idOrder) || idOrder - 1 <  0 || idOrder - 1 >= users[id].Orders.Count)
            {
                Console.WriteLine("    You enter invalid id  ");
                Console.Write("    Enter order id: ");
                idStr = Console.ReadLine();
            }
            idOrder = idOrder - 1;

            int Change = 0;
            while (Change != (int)ChangeUser.Exit)
            {
                Console.WriteLine("           ---Change status of user order ---       ");
                Console.WriteLine("  |0|  Change status to New                        |1|  Change status to Canceled by Administrator");
                Console.WriteLine("  |2|  Change status to Payment and Received       |3|  Change status to Sent ");
                Console.WriteLine("  |4|  Change status to Received                   |5|  Completed");
                Console.WriteLine("  |6|  Close menu");
                Change = Convert.ToInt32(Console.ReadLine());
                switch (Change)
                {
                    case (int)Status.New:
                        users[id].Orders[idOrder].CurrentStatus = Status.New;
                        break;
                    case (int)Status.CanceledByAdministrator:
                        users[id].Orders[idOrder].CurrentStatus = Status.CanceledByAdministrator;
                        break;
                    case (int)Status.Sent:
                        users[id].Orders[idOrder].CurrentStatus = Status.Sent;
                        break;
                    case (int)Status.Received:
                        users[id].Orders[idOrder].CurrentStatus = Status.Received;
                        break;
                    case (int)Status.Completed:
                        users[id].Orders[idOrder].CurrentStatus = Status.Completed;
                        break;
                    case (int)Status.PaymentReceived:
                        users[id].Orders[idOrder].CurrentStatus = Status.PaymentReceived;
                        break;
                    default:
                        Change = (int)ChangeUser.Exit;
                        break;
                }

            }
            Console.WriteLine();
        }

        public void CreateNewOrder(List<Good> goods, List<RegisterUser> users)
        {
            Console.WriteLine("      ---Choose user---     ");
            DisplayUsersInformation(users);

            Console.Write("    Enter  user id: ");
            string idStr = Console.ReadLine();
            int id;

            while (!Int32.TryParse(idStr,out id) || id - 1 < 0 || id - 1>= users.Count)
            {
                Console.WriteLine("    You entered invalid id  ");
                Console.Write("    Enter user id: ");
                idStr = Console.ReadLine();
            }

            id = id - 1;

            Console.WriteLine("  --Choose category of goods--  ");
            Console.WriteLine();

            List<string> category = RegisterUserRole.SelectCategory(goods);

            for (int i = 0; i < category.Count; i++)
            {
                Console.WriteLine($" {i + 1}.  {category[i]}");
            }

            Console.Write("  Input number of category ");
            string chosen = Console.ReadLine();
            int chosenNumber;

            while (!Int32.TryParse(chosen, out chosenNumber) || chosenNumber - 1 < 0 || chosenNumber - 1 >= category.Count)
            {
                Console.WriteLine("  You input invalid number or number out of range");
                chosen = Console.ReadLine();
            }
            chosenNumber = chosenNumber - 1;
            chosen = category[chosenNumber];

            List<Good> goodsByChosenCategory = new List<Good>();

            Console.WriteLine("  --Choose good--  ");
            Console.WriteLine();

            int j = 0;
            foreach (Good good in goods)
            {
                if (good.Category == chosen)
                {
                    Console.WriteLine($" {j + 1}.  {good.Name}");
                    Console.WriteLine($" Description: {good.Description}");
                    Console.WriteLine($" Cost: {good.Cost}");
                    Console.WriteLine();
                    goodsByChosenCategory.Add(new Good { Name = good.Name, Description = good.Description, Cost = good.Cost });
                    j++;
                }
            }

            Console.Write("  Input number of good ");
            chosen = Console.ReadLine();
            while (!Int32.TryParse(chosen, out chosenNumber) || chosenNumber - 1< 0 || chosenNumber - 1 >= goodsByChosenCategory.Count)
            {
                Console.WriteLine("  You input invalid number or number out of range");
                chosen = Console.ReadLine();
            }
            chosenNumber = Convert.ToInt32(chosen) - 1;

            Console.WriteLine();

            users[id].Orders.Add(new Order { good = goodsByChosenCategory[chosenNumber], CurrentStatus = Status.New, CreationDate = DateTime.Now });

        }
    }
}
