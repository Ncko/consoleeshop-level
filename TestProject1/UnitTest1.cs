using NUnit.Framework;
using ClassLibrary1;
using System.Collections.Generic;
using System;
using ClassLibrary1.Exceptions;
using Moq;

namespace TestProject1
{
    public class Tests
    {
        [Test]
        public void SearchGoodByName_ReturnSearchedGood()
        {
            List<Good> currentGoods = new List<Good>
            {
                new Good { Category = "Laptop" , Name = "Asus 1135", Cost = 2400.0, Description = "New model. Quick processing data" },
                new Good {Category = "Laptop" , Name = "Lenovo 176" , Cost = 3000.0, Description=" You haven't seen this before "},
                new Good {Category ="Laptop" , Name = "Hp 90011", Cost = 2900.0 , Description = " Old but gold"},
                new Good {Category=" Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"}
            };

            test1(currentGoods, currentGoods[2], "Hp 90011");
            test1(currentGoods, currentGoods[3], "Bosh 13");
            test1(currentGoods, currentGoods[0], "Asus 1135");
            void test1(List<Good> goods, Good expectedGood, string name)
            {
                var actual = new RegisterUserRole().SearchGoodByName(goods, name);
                Assert.AreEqual(expectedGood, actual, " Method SearchGoodByName works incorrectly. Good was not founded.");

            }

        }
        [Test]
        public void SearchGoodByName_InputNotExistedGood_ThrowNotFoundedGoodException()
        {
            List<Good> currentGoods = new List<Good>
            {
                new Good {Category ="Laptop" , Name = "Hp 90011", Cost = 2900.0 , Description = " Old but gold"},
                new Good {Category=" Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"},
                new Good {Category = "Chair" , Name ="Velmont Classic" , Cost = 1800.0 , Description = "Yellow chair"},
                new Good {Category = "TV" , Name = "Samsung ArhGalaxy" , Cost = 2600.0 , Description =" No"},
                new Good {Category = "TV", Name = "Toshiba Ussr", Cost = 1900.0, Description = "OLd but gold"}
            };

            test2(currentGoods, new NotFoundedGoodException("Name"), "Name");
            test2(currentGoods, new NotFoundedGoodException("Bosh"), "Bosh");
            test2(currentGoods, new NotFoundedGoodException("Velmont"), "Velmont");

            void test2(List<Good> goods, NotFoundedGoodException expected, string name)
            {
                var expectedTypeError = typeof(NotFoundedGoodException);
                var expectedMessage = $"Good with name:{name} doesn't exist";

                Exception ex = Assert.Catch(() => new RegisterUserRole().SearchGoodByName(goods, name));

                Assert.AreEqual(expectedTypeError, ex.GetType(),message: " Method SearchGoodByName works incorrectly. Must throw new NotFoundedGoodException.");
                Assert.AreEqual(expectedMessage, ex.Message, message: " Method SearchGoodByName works incorrectly. Must throw new NotFoundedGoodException.");
            }
        }

        [Test]
        public void SearchUserByNickName_ReturnUser()
        {
            List<RegisterUser> users = new List<RegisterUser>
            {
                new RegisterUser{
                    Name = "Pasha" ,
                    Surname = "Sheychenko",
                    NickName="First",
                    Password = "password",
                    Email ="first@gmail.com"
                },
                new RegisterUser{
                    Name = "Katerina",
                    Surname = "Phokina" ,
                    NickName = "kat" ,
                    Password = "password1",
                    Email ="kat@gmail.com"
                },
                new RegisterUser
                {
                    Name = "Oleg",
                    Surname = "Jugostransky" ,
                    NickName = "yeger" ,
                    Password = "password2",
                    Email = "Oleg@mail.Com"
                },
                new RegisterUser
                {
                    Name="Philip",
                    Surname = "Kirkorov",
                    NickName = "Kirkor",
                    Password = "111678",
                    Email = "email"
                }
            };

            test3(users, 0, "First");
            test3(users, 1, "kat");
            test3(users, 3, "Kirkor");
            test3(users, 2, "yeger");

            void test3(List<RegisterUser> users, int expected, string name)
            {
                var actual = new RegisterUserRole(name).FindMyself(users);

                Assert.AreEqual(expected, actual, "Method FindMyself works incorectly. Must return index of place of the user");
            }
        }

        [Test]
        public void SearchUserByNickName_ThrowNewNotFoundedUserException()
        {
            List<RegisterUser> users = new List<RegisterUser>
            {
                new RegisterUser{
                    Name = "Pasha" ,
                    Surname = "Sheychenko",
                    NickName="First",
                    Password = "password",
                    Email ="first@gmail.com"
                },
                new RegisterUser{
                    Name = "Katerina",
                    Surname = "Phokina" ,
                    NickName = "kat" ,
                    Password = "password1",
                    Email ="kat@gmail.com"
                },
                new RegisterUser
                {
                    Name = "Oleg",
                    Surname = "Jugostransky" ,
                    NickName = "yeger" ,
                    Password = "password2",
                    Email = "Oleg@mail.Com"
                },
                new RegisterUser
                {
                    Name="Philip",
                    Surname = "Kirkorov",
                    NickName = "Kirkor",
                    Password = "111678",
                    Email = "email"
                }
            };

            test4(users, new NotFoundedUserException("Name"), "Name");
            test4(users, new NotFoundedUserException("Bosh"), "Bosh");
            test4(users, new NotFoundedUserException("Velmont"), "Velmont");

            void test4(List<RegisterUser> users, NotFoundedUserException expected, string name)
            {

                var expectedTypeError = typeof(NotFoundedUserException);
                var expectedMessage = $" User with name:{name} doesn't exist";

                Exception ex = Assert.Catch(() => new RegisterUserRole(name).FindMyself(users));

                Assert.AreEqual(expectedTypeError, ex.GetType(), message: " Method SearchGoodByName works incorrectly. Must throw new NotFoundedGoodException.");
                Assert.AreEqual(expectedMessage, ex.Message, message: " Method SearchGoodByName works incorrectly. Must throw new NotFoundedGoodException.");

            }
        }
        [Test]
        public void SelectCategoryFromGoods_ReturnListOfCategory()
        {
            List<Good> goods1 = new List<Good>
            {
                new Good { Category = "Laptop" , Name = "Asus 1135", Cost = 2400.0, Description = "New model. Quick processing data" },
                new Good {Category = "Laptop" , Name = "Lenovo 176" , Cost = 3000.0, Description=" You haven't seen this before "},
                new Good {Category ="Laptop" , Name = "Hp 90011", Cost = 2900.0 , Description = " Old but gold"},
                new Good {Category="Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"},
                new Good {Category = "Chair" , Name ="Velmont Classic" , Cost = 1800.0 , Description = "Yellow chair"},
                new Good {Category = "TV" , Name = "Samsung ArhGalaxy" , Cost = 2600.0 , Description =" No"},
                new Good {Category = "TV", Name = "Toshiba Ussr", Cost = 1900.0, Description = "OLd but gold"}
            };
            List<string> expected1 = new List<string> { "Laptop", "Washer", "Chair", "TV" };

            List<Good> goods2 = new List<Good>
            {
                new Good {Category="Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"},
                new Good {Category = "Chair" , Name ="Velmont Classic" , Cost = 1800.0 , Description = "Yellow chair"},
                new Good {Category = "TV" , Name = "Samsung ArhGalaxy" , Cost = 2600.0 , Description =" No"},
                new Good {Category = "TV", Name = "Toshiba Ussr", Cost = 1900.0, Description = "OLd but gold"}
            };

            List<string> expected2 = new List<string> { "Washer", "Chair", "TV" };

            List<Good> goods3 = new List<Good>
            {
                new Good { Category = "Laptop" , Name = "Asus 1135", Cost = 2400.0, Description = "New model. Quick processing data" },
                new Good {Category = "Laptop" , Name = "Lenovo 176" , Cost = 3000.0, Description=" You haven't seen this before "},
                new Good {Category ="Laptop" , Name = "Hp 90011", Cost = 2900.0 , Description = " Old but gold"},
                new Good {Category="Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"},
                new Good {Category = "Chair" , Name ="Velmont Classic" , Cost = 1800.0 , Description = "Yellow chair"},
            };

            List<string> expected3 = new List<string> { "Laptop", "Washer", "Chair" };

            test5(goods1, expected1);
            test5(goods2, expected2);
            test5(goods3, expected3);

            void test5(List<Good> goods, List<string> expectedCategory)
            {
                var actual = RegisterUserRole.SelectCategory(goods);
                Assert.AreEqual(expectedCategory, actual, " Method works incorrectly. Must return list of strings of category");
            }
        }

        [Test]
        public void SelectCategoryFromGoods_ThrowArgumentNullException()
        {
            var expectedTypeError = typeof(ListOfGoodNullException);

            Exception ex = Assert.Catch(() => RegisterUserRole.SelectCategory(null));

            Assert.AreEqual(expectedTypeError, ex.GetType(),
                message: " Method SelectCategory should throw new ArgumentNullException if list of good is null ");
        }

        [Test]
        public void CheckSettingMethodsInDelegatesInRegisterRole()
        {
            var actual = new RegisterUserRole();

            Assert.IsNotNull(actual._cancelOrder);
            Assert.IsNotNull(actual._changePersonalInformation);
            Assert.IsNotNull(actual._displayMyOrders);
            Assert.IsNotNull(actual._createNewOrder);
            Assert.IsNotNull(actual._setStatusReceived);
            Assert.IsNotNull(actual._signOut);

        }

        [Test]
        public void CheckSettingMethodsInDelegatesInAdministratorRole()
        {
            var actual = new AdministratorUserRole();

            Assert.IsNotNull(actual._createNewOrder);
            Assert.IsNotNull(actual._changeStatusOfOrder);
            Assert.IsNotNull(actual._displayAndChangeUser);
            Assert.IsNotNull(actual._addGood);
            Assert.IsNotNull(actual._changeInformationGood);
            Assert.IsNotNull(actual._signOut);

        }

        [Test]
        public void CheckSettingMethodsInDelegatesInGuestRole()
        {
            var actual = new GuestUserRole();

            Assert.IsNotNull(actual._registration);
            Assert.IsNotNull(actual._signIn);
            Assert.IsNotNull(actual._signInAsAdmin);

        }


    }
}