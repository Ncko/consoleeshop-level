﻿using System;
using ClassLibrary1;
using System.Collections.Generic;


namespace ConsoleApp1
{
    static class Program
    {
        static void Main(string[] args)
        {
            List<Good> goods = new List<Good>
            {
                new Good { Category = "Laptop" , Name = "Asus 1135", Cost = 2400.0, Description = "New model. Quick processing data" },
                new Good {Category = "Laptop" , Name = "Lenovo 176" , Cost = 3000.0, Description=" You haven't seen this before "},
                new Good {Category ="Laptop" , Name = "Hp 90011", Cost = 2900.0 , Description = " Old but gold"},
                new Good {Category="Washer" , Name ="Bosh 13", Cost = 3000.0, Description = "  I dont have any idea what write in this string"},
                new Good {Category = "Chair" , Name ="Velmont Classic" , Cost = 1800.0 , Description = "Yellow chair"},
                new Good {Category = "TV" , Name = "Samsung ArhGalaxy" , Cost = 2600.0 , Description =" No"},
                new Good {Category = "TV", Name = "Toshiba Ussr", Cost = 1900.0, Description = "OLd but gold"}
            };

            List<RegisterUser> users = new List<RegisterUser>
            {
                new RegisterUser{
                    Orders = new List<Order>
                {
                    new Order{ good = goods[2], CreationDate = DateTime.Now,CurrentStatus = Status.New},
                    new Order{ good  = goods[1],CreationDate = DateTime.Now,CurrentStatus = Status.PaymentReceived}
                },
                    Name = "Pasha" ,
                    Surname = "Sheychenko",
                    NickName="First",
                    Password = "password",
                    Email ="first@gmail.com"
                },
                new RegisterUser{
                    Orders = new List<Order>
                    {
                        new Order{good = goods[3], CreationDate = DateTime.Now,CurrentStatus = Status.New},
                        new Order{good = goods[0],CreationDate = DateTime.Now,CurrentStatus = Status.Completed}
                    },
                    Name = "Katerina",
                    Surname = "Phokina" ,
                    NickName = "kat" ,
                    Password = "password1",
                    Email ="kat@gmail.com"
                },
                new RegisterUser
                {
                    Orders = new List<Order>
                    {
                        new Order{good = goods[4],CreationDate =DateTime.Now,CurrentStatus = Status.Received}
                    },
                    Name = "Oleg",
                    Surname = "Jugostransky" ,
                    NickName = "yeger" ,
                    Password = "password2",
                    Email="Oleg@mail.Com"
                }
            };

            List<AdministratorUser> admins = new List<AdministratorUser>
            {
                new AdministratorUser{
                    Name = "Pavel" ,
                    Surname = "Alexandrovic",
                    NickName = "admin1",
                    Password="adminPassword",
                    Email="pavel@gmail.com"
                },
                new AdministratorUser{
                    Name = "Svetlana" ,
                    Surname = " Phedorovna",
                    NickName= " siryk" ,
                    Password = "syrikPassword" ,
                    Email = "syrik@gmal.com"
                }
            };



            UserRole user = new GuestUserRole();
            Context context = new Context((GuestUserRole)user);

            Console.WriteLine("              && WEB - SHOP &&           \n");

            while (true)
            {
                context.DisplayMenu(users, admins, goods);
            }
        }
    }
}
